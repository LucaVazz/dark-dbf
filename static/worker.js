self.addEventListener('install', function(event) {
    console.log('[Service Worker] Handled install event')
})

self.addEventListener('fetch', function(event) {
    console.log('[Service Worker] Handled fetch event')
    event.respondWith(async function() {
        return fetch(event.request);  // fetch from network
    }())
})
