/// SERVICE WORKER: (for full add-to-home-screen compatibility)

if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('/worker.js', { scope: '/' })
        .then((reg) => {
            if(reg.installing) {
                console.log('[App] Service worker installing...')
            } else if(reg.waiting) {
                console.log('[App] Service worker installed')
            } else if(reg.active) {
                console.log('[App] Service worker active')
            }

        }).catch((error) => {
            console.log('[App] Service worker registration failed with ' + error)
        })
    ;
}


/// LOADING ANIMATION:

const loadingOverlayEl = document.getElementById('loading-overlay')

// outgoing load:
window.addEventListener('beforeunload', () => {
    loadingOverlayEl.classList.remove('is-hidden');
});


/// CUSTOM PULL-TO-REFRESH:

let _startY;
let _startedAtTop = false, _shouldRefresh = false;
const contentContainerEl = document.getElementById('content-container');
const refreshPromptEl = document.getElementById('refresh-prompt');

contentContainerEl.addEventListener('touchstart', evt => {
    _startY = evt.touches[0].pageY;
    _startedAtTop = (document.scrollingElement.scrollTop === 0);
}, {passive: true});

contentContainerEl.addEventListener('touchmove', evt => {
    let currentY = evt.touches[0].pageY;
    _shouldRefresh = (
        _startedAtTop
        && document.scrollingElement.scrollTop === 0 // i.e. still at the top
        && currentY > _startY // i.e. scrolled down
    );

    if (_shouldRefresh) {
        refreshPromptEl.classList.add('show')
    } else {
        refreshPromptEl.classList.remove('show')
    }
}, {passive: true});

contentContainerEl.addEventListener('touchend', evt => {
    if (_shouldRefresh) {
        window.location.reload(true);
    }
}, {passive: true});
