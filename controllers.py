import datetime

from flask import request, render_template, make_response, redirect, url_for

from data_services.abbrev import abbrev_service
from data_services.dbf import dbf_service
from data_services.dbf.types.errors import UnknownStationError, UnknownEntryError
from data_services.dbf.types.message_type import MessageType
from data_services.photo import photo_service
from data_services.statistics import statistics_service
from data_services.wagon import wagon_service
from data_services.wagon.types.wagon_speciality import WagonSpeciality


def _get_favs() -> [str]:
    favs_str = request.cookies.get('favs', '')
    if len(favs_str) == 0:
        favs = []
    else:
        favs = favs_str.split(',')
    return favs

def _set_favs(resp, favs_abbrevs: [str]):
    resp.set_cookie(
        'favs',
        ','.join(favs_abbrevs),
        expires=datetime.datetime.now() + datetime.timedelta(days=365)
        # if they don't come back in one year, its their fault for losing favourites
    )
    return resp


def _format_ul(entries: [str]) -> str:
    return '<ul><li>' + '</li><li>'.join(entries) + '</li></ul>'


def home():
    favs_abbrevs = _get_favs()
    favs = [
        {
            'abbrev': abbrev,
            'name': abbrev_service.get_name(abbrev),
            'photo_url': photo_service.get_photo_url(abbrev)
        }
        for abbrev in favs_abbrevs
    ]

    resp = make_response(render_template(
        'home.jinja2', favs=favs
    ))
    return _set_favs(resp, favs_abbrevs)

def home_search():
    query = request.args.get('query', '')
    return render_template(
        'home_search.jinja2', query=query, results=abbrev_service.list_matching_entries(query)
    )


def station_list(bs_abbrev: str):
    try:
        data = dbf_service.get_data(bs_abbrev)
        entries = dbf_service.parse_to_entry_list(data)
    except UnknownStationError:
        return (
            render_template(
                'error.jinja2',
                title='Station hat keine Daten',
                msg='Für die angegebene Station wurden keine Echtzeit-Daten gefunden. <br/><br/>' +
                    'Mögliche Ursachen:' +
                    _format_ul([
                        'Falsches Kürzel eingegeben.',
                        'Diese Station ist ein Betriebshof, für die keine Daten verfügbar gemacht werden.'
                    ])
            ),
            404
        )
    name = abbrev_service.get_name(bs_abbrev)

    return render_template(
        'station_list.jinja2', bs_abbrev=bs_abbrev, name=name, entries=entries
    )


def station_details(bs_abbrev: str):
    name = abbrev_service.get_name(bs_abbrev)
    photo_url = photo_service.get_photo_url(bs_abbrev)
    is_fav = bs_abbrev in _get_favs()

    return render_template(
        'station_details.jinja2', bs_abbrev=bs_abbrev, name=name, photo_url=photo_url, is_fav=is_fav
    )

def station_fav_add(bs_abbrev: str):
    favs = _get_favs()
    if bs_abbrev not in favs:
        favs.append(bs_abbrev)

    resp = make_response(redirect(url_for(
        'route_station', bs_abbrev=bs_abbrev, mode='details'
    )))
    return _set_favs(resp, favs)

def station_fav_remove(bs_abbrev: str):
    favs = _get_favs()
    if bs_abbrev in favs:
        favs.remove(bs_abbrev)

    resp = make_response(redirect(url_for(
        'route_station', bs_abbrev=bs_abbrev, mode='details'
    )))
    return _set_favs(resp, favs)


def station_statistics(bs_abbrev: str):
    name = abbrev_service.get_name(bs_abbrev)
    data = dbf_service.get_data(bs_abbrev)
    stats = statistics_service.create_statistics(data)

    return render_template(
        'station_statistics.jinja2', bs_abbrev=bs_abbrev, name=name, stats=stats
    )


def station_dashboard(bs_abbrev: str):
    name = abbrev_service.get_name(bs_abbrev)
    data = dbf_service.get_data(bs_abbrev)

    entries = dbf_service.parse_to_entry_list(data)
    stats = statistics_service.create_statistics(data)

    tile_data = [
        {
            'abbrev': bs_abbrev,
            'name': abbrev_service.get_name(bs_abbrev),
            'photo_url': photo_service.get_photo_url(bs_abbrev)
        }
    ]

    time = datetime.datetime.now().strftime('%H:%M')

    return render_template(
        'station_dashboard.jinja2',
        bs_abbrev=bs_abbrev, name=name, entries=entries, stats=stats, tile_data=tile_data, time=time
    )


def entry_details(bs_abbrev: str, entry_number: str):
    try:
        data_list = dbf_service.get_data(bs_abbrev)
        entry = dbf_service.parse_to_one_entry(data_list, entry_number)
    except UnknownEntryError:
        return (
            render_template(
                'error.jinja2',
                title='Fahrt nicht gefunden',
                msg='Für die angegebene Fahrt wurden keine Echtzeit-Daten gefunden. <br/><br/>' +
                    'Mögliche Ursachen:' +
                    _format_ul([
                        'Der Zug ist bereits (laut DB-System) von der angegebenen Station abgefahren.',
                        'Falsche Fahrt-Nummer oder falsches Stations-Kürzel eingegeben.'
                    ])
            ),
            404
        )

    time = entry.scheduled_departure if entry.scheduled_departure is not None else entry.scheduled_arrival
    [time_hour, time_minute] = time.split(':')
    wagon_sequence = wagon_service.get_wagon_sequence(entry.number, int(time_hour), int(time_minute))

    return render_template(
        'entry_details.jinja2', bs_abbrev=bs_abbrev, entry_number=entry_number,
        entry=entry, message_type=MessageType,
        wagon_sequence=wagon_sequence, wagon_speciality=WagonSpeciality
    )
