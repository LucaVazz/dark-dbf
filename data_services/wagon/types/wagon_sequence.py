from data_services.wagon.types.wagon import Wagon


class WagonSequence:
    def __init__(self, wagons: [Wagon], direction: int):
        self.wagons = wagons
        self.direction = direction
        """1: first wagon is driving, -1: last wagon is driving"""
