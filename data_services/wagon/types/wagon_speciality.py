from enum import Enum, auto


class WagonSpeciality(Enum):
    BISTRO = auto()
    RESTAURANT = auto()
    WHEELCHAIR_PLACE = auto()
    SLEEP = auto()
    LOCKED = auto()
    BIKE_ROOM = auto()
    COMPARTMENTS = auto()
