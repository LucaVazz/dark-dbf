from datetime import datetime, timedelta
from typing import Union

import requests

from data_services.wagon.types.wagon import Wagon
from data_services.wagon.types.wagon_sequence import WagonSequence
from .types.wagon_speciality import WagonSpeciality


def get_wagon_sequence(train_number: str, time_hour: int, time_minutes: int) -> Union[WagonSequence, None]:
    now = datetime.now()
    if (
        time_hour < now.hour or
        (time_hour == now.hour and time_minutes < now.minute)
    ):
        now += timedelta(days=1)  # time lays in next day
    date_str = '%02d%02d%02d%02d%02d'%(now.year, now.month, now.day, time_hour, time_minutes)

    r = requests.get('https://www.apps-bahn.de/wr/wagenreihung/1.0/%s/%s'%(train_number, date_str))
    if r.status_code != 200:
        return None
    data = r.json()

    if data.get('error') is not None:
        return None

    data = data.get('data').get('istformation')
    direction = 1 if data.get('fahrtrichtung') == 'VORWAERTS' else -1

    wagons = []
    for wagon_group in data.get('allFahrzeuggruppe'):
        pos = 1
        for wd in wagon_group.get('allFahrzeug'):
            sector = wd.get('fahrzeugsektor')
            number = wd.get('wagenordnungsnummer')
            seat_class = ''
            specialities = []
            is_control = False

            if wd.get('kategorie') == 'TRIEBKOPF' or wd.get('kategorie') == 'LOK':
                is_control = True

            if wd.get('status') != 'OFFEN':
                specialities.append(WagonSpeciality.LOCKED)

            type_str = wd.get('fahrzeugtyp')

            # main type - see https://de.wikipedia.org/wiki/Gattungszeichen_deutscher_Eisenbahnwagen#Hauptgattungszeichen_für_Reisezugwagen
            if type_str.startswith('WL'):
                specialities.append(WagonSpeciality.SLEEP)
                type_str = type_str[2:]

            if type_str.startswith('WR'):
                specialities.append(WagonSpeciality.RESTAURANT)
            else:
                if type_str.startswith('A'):
                    seat_class = '1.'
                    type_str = type_str[1:]
                    if type_str.startswith('B'):
                        seat_class += ' & 2.'
                        type_str = type_str[1:]
                elif type_str.startswith('B'):
                    seat_class = '2.'
                    type_str = type_str[1:]

                if type_str.startswith('R'):
                    specialities.append(WagonSpeciality.BISTRO)

            # sub types - see https://de.wikipedia.org/wiki/Gattungszeichen_deutscher_Eisenbahnwagen#Nebengattungszeichen_für_Reisezugwagen
            if 'b' in type_str:
                specialities.append(WagonSpeciality.WHEELCHAIR_PLACE)
            if 'c' in type_str:
                specialities.append(WagonSpeciality.SLEEP)
            if 'd' in type_str:
                specialities.append(WagonSpeciality.BIKE_ROOM)
            if 'p' not in type_str:
                specialities.append(WagonSpeciality.COMPARTMENTS)

            wagons.append(Wagon(number, sector, seat_class, specialities, pos, is_control))
            pos = 0
        wagons[-1].position = -1

    return WagonSequence(wagons, direction)
