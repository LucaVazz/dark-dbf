from .message import Message
from .short_entry import ShortEntry
from .stop import Stop


class FullEntry(ShortEntry):
    def __init__(self,
        short: ShortEntry,
        route: [Stop], messages: [Message]
    ):
        super().__init__(
            short.number, short.name, short.class_, short.destination, short.via,
            short.scheduled_arrival, short.scheduled_departure, short.scheduled_platform,
            short.delay_arrival, short.delay_departure, short.actual_platform,
            short.is_cancelled, len(messages) > 1
        )

        self.route = route
        self.messages = messages
