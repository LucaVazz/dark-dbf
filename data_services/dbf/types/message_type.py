from enum import Enum


class MessageType(Enum):
    delay               = 1
    quality_of_service  = 2
