class Stop:
    def __init__(self, name: str, is_additional: bool, is_cancelled: bool):
        self.name = name
        self.is_additional = is_additional
        self.is_cancelled = is_cancelled
