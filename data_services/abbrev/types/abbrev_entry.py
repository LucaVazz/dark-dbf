from data_services.photo import photo_service


class AbbrevEntry:
    def __init__(self, abbrev: str, name: str, typ: str):
        self.abbrev = abbrev
        self.name = name
        self.typ = typ
        self.photo_url = photo_service.get_photo_url(abbrev)